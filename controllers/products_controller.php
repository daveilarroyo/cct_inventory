<?php
class ProductsController extends AppController {

	var $name = 'Products';
	function beforeFilter(){
		$status = array(
				'active'=>'Active',
				'low'=>'Low',
				'none'=>'Out of Stock'
			);
		$this->set(compact('status'));
		parent::beforeFilter();
	}
	function index($page=1,$sort=null,$direction=null,$filter=null){
		if(isset($_GET)){
			$conditions = array('OR'=>array());
			$keyword=null;
			$params = array();
			foreach($_GET as $key=>$value){
				if($key!='url') array_push($params,$key.'='.$value);
				if(!$value || $key =='url') continue;
				switch($key){
					case 'keyword':
						$keyword = $value;
						$value = '%'.$value.'%';
						$fields =  array('particular','part_no','description');
						foreach($fields as $field){
							$cond = array('Product.'.$field.' LIKE'=>$value);
							array_push($conditions['OR'],$cond);
						}
					break;
					case 'page':
						$page =  $value;
					break;
					
					
				}
			}
			if($_GET['url']=='products?status=low'){
				$cond = 'Product.soh_quantity <= Product.min_quantity';
				array_push($conditions['OR'],$cond);
			}
			$params = '?'.implode('&',$params);
			if(!count($conditions['OR'])) unset($conditions['OR']);
			 $this->paginate = array(
					'Product'=>array('conditions' => $conditions,'limit'=>10,'page'=>$page,'sort'=>$sort,'direction'=>$direction,'cache'=>'default'),
				);		
			if (isset($this->params['requested'])) {
				return $this->paginate();
			}
			$this->set('products', $this->paginate());
			$this->set(compact('keyword','params','sort','direction'));
			
		}
	}
	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid product', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('product', $this->Product->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Product->create();
			if ($this->Product->save($this->data)) {
				$this->Session->setFlash(__('The product has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Duplicate found.', true));
			}
		}
		$categories = $this->Product->Category->find('list');
		$this->set(compact('categories'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid product', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Product->save($this->data)) {
				$this->Session->setFlash(__('The product has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Product->read(null, $id);
		}
		$categories = $this->Product->Category->find('list');
		$this->set(compact('categories'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for product', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Product->delete($id)) {
			$this->Session->setFlash(__('Product deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Product was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
