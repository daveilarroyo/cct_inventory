<?php echo $this->Html->script('bower_components/angular/angular.min');?>
<?php echo $this->Html->script('product');?>
<div class="products form" ng-app="ProductModule">
<?php echo $this->Form->create('Product',array('ng-controller'=>'ProductController'));?>
	<fieldset>
		<legend><?php __('Edit Product'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('category_id');
		echo $this->Form->input('status',array('options'=>$status));
		echo $this->Form->input('particular');
		echo $this->Form->input('part_no');
		echo $this->Form->input('unit');
		echo $this->Form->input('description');
		echo $this->Form->input('capital',array('ng-model'=>'capital','label'=>'Unit Cost'));
		echo $this->Form->input('markup',array('ng-model'=>'markup'));
		echo $this->Form->input('price',array('ng-model'=>'price'));
		echo $this->Form->input('soh_quantity',array('label'=>'Stock on Hand'));
		echo $this->Form->input('min_quantity');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Product.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Product.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Products', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Categories', true), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category', true), array('controller' => 'categories', 'action' => 'add')); ?> </li>
	</ul>
</div>