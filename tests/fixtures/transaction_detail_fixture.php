<?php
/* TransactionDetail Fixture generated on: 2016-01-29 14:21:38 : 1454073698 */
class TransactionDetailFixture extends CakeTestFixture {
	var $name = 'TransactionDetail';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'transaction_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'product_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'price' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'transaction_id' => 1,
			'product_id' => 1,
			'quantity' => 1,
			'price' => 1,
			'amount' => 1
		),
	);
}
