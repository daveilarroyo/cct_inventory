<?php echo $this->Html->css('transaction.generic');?>
<?php echo $this->Html->script('bower_components/angular/angular.min');?>
<?php echo $this->Html->script('bower_components/jquery/dist/jquery.min');?>
<?php echo $this->Html->script('sales');?>
<div ng-app="SalesModule">
	<div ng-controller="SalesController" ng-init="initialize()">
		<div class="transactions form">
			<?php echo $this->Form->create('Transaction',array('action'=>'add'));?>
				<fieldset>
					<legend><?php __('Sales Form'); ?></legend>
				<?php
					echo $this->Form->input('Transaction.type',array('type'=>'hidden','value'=>'sales'));
					echo $this->Form->input('Transaction.total_amount',array('div'=>'hidden', 'ng-model'=>'TotalAmount'));
					echo $this->Form->input('Transaction.net_amount',array('div'=>'hidden', 'ng-model'=>'NetAmount'));
					echo $this->Form->input('Transaction.cash_amount',array('div'=>'hidden', 'ng-model'=>'CashAmount'));
					echo $this->Form->input('Transaction.tax_amount',array('div'=>'hidden', 'ng-model'=>'TaxAmount'));
					echo $this->Form->input('Transaction.transaction_date',array('div'=>'hidden','type'=>'text','ng-model'=>'TransacFormattedDate'));
					echo $this->Form->input('Transaction.transaction_time',array('div'=>'hidden','type'=>'text','ng-model'=>'TransacFormattedTime'));
					echo $this->Form->input('Product.name',array('label'=>false,'empty'=>'Enter Product','options'=>$products,'ng-change'=>'selectProduct()', 'ng-model'=>'Product'));
				?>
				<table>
					<tr>
						<th>Product</th>
						<th>Quantity</th>
						<th>Price</th>
						<th>Amount</th>
						<th>&nbsp;</th>
					</tr>
					<tr ng-repeat="detail in TransactionDetails track by $index">
						<td><?php 
								echo $this->Form->input('Product.name',array('label'=>false,'name'=>'data[Product][{{$index}}][name]','readonly'=>true, 'ng-model'=>'detail.particular'));
								echo $this->Form->input('TransactionDetail.product_id',array('div'=>'hidden','label'=>false,'name'=>'data[TransactionDetail][{{$index}}][product_id]','ng-model'=>'detail.id'));
							?></td>
						<td><?php echo $this->Form->input('TransactionDetail.quantity',array('label'=>false,'class'=>'number','name'=>'data[TransactionDetail][{{$index}}][quantity]','readonly'=>false, 'ng-model'=>'detail.quantity', 'ng-change'=>'updateAmount(detail)'));?></td>
						<td><?php echo $this->Form->input('TransactionDetail.price',array('label'=>false,'class'=>'number','name'=>'data[TransactionDetail][{{$index}}][price]','readonly'=>true, 'ng-model'=>'detail.price'));?></td>
						<td><?php 
								echo $this->Form->input('TransactionDetail.amount',array('label'=>false,'class'=>'number','name'=>'data[TransactionDetail][{{$index}}][display_amount]','readonly'=>true, 'ng-model'=>'detail.display_amount'));
								echo $this->Form->input('TransactionDetail.amount',array('div'=>'hidden','label'=>false,'name'=>'data[TransactionDetail][{{$index}}][amount]', 'ng-model'=>'detail.amount'));
							?></td>
						<td><div class="submit"><button type="button" ng-click="removeItem($index)">&times;</button></div></td>
					</tr>
				</table>
				</fieldset>
			<?php echo $this->Form->end();?>
			</div>
			<div class="actions">
				<h5>
					<div class="input text"> 
						<label for="TransactionTransactionDateTime">Date/Time</label>
						<input type="date" name="data[Transaction][transaction_date]" ng-model="TransacDate"/>
						<input type="time" name="data[Transaction][transaction_time]" ng-model="TransacTime"/>
					</div>
				</h5>
				<ul>
					<li><a>Total: <b>{{TotalAmount | currency:'P'}}</b></a></li>
					<li><a>Net: <b>{{NetAmount | number:2}}</b></a></li>
					<li><a>Change: <b>{{ChangeAmount |number:2 }}</b></a></li>
					<li>
						<?php
						echo $this->Form->input('Transaction.tax',array('class'=>'number','ng-model'=>'TaxAmount'));
						echo $this->Form->input('Transaction.cash',array('class'=>'number','ng-model'=>'CashAmount'));
					?>
					</li>
					
				</ul>
				<form action="">
					<div class="submit"><button type="button" ng-click="processSales()">Submit</button></div>
				</form>
			</div>
	</div>
</div>
