<div class="transactions index">
	<h2><?php __('Transactions');?></h2>
	<?php 
		echo $this->element('transaction_filter_widget'); 
		echo $this->element('transaction_print_widget'); 
		$this->Paginator->options(array('url' =>array('sort'=>$sort,'direction'=>$direction,'from'=>$from,'to'=>$to)));
	?>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th>Product</th>
			<th>Encoder</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Amount</th>
			<th>Date</th>
	</tr>
	<?php
	$i = 0;
	foreach ($transactions as $transaction):
	foreach($transaction['TransactionDetail'] as $detail):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $detail['Product']['particular']; ?>&nbsp;</td>
		<td><?php echo $transaction['Transaction']['username']; ?>&nbsp;</td>
		<td><?php echo $detail['quantity']; ?>&nbsp;</td>
		<td><?php echo	$detail['price']; ?>&nbsp;</td>
		<td><?php echo	$detail['amount']; ?>&nbsp;</td>
		<td><?php echo $transaction['Transaction']['transaction_date']; ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Transaction', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Transaction Details', true), array('controller' => 'transaction_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction Detail', true), array('controller' => 'transaction_details', 'action' => 'add')); ?> </li>
	</ul>
</div>