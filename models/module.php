<?php
class Module extends AppModel {
	var $name = 'Module';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'ModuleUser' => array(
			'className' => 'ModuleUser',
			'foreignKey' => 'module_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
