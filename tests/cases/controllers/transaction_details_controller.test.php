<?php
/* TransactionDetails Test cases generated on: 2016-01-29 14:22:05 : 1454073725*/
App::import('Controller', 'TransactionDetails');

class TestTransactionDetailsController extends TransactionDetailsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class TransactionDetailsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.transaction_detail', 'app.transaction', 'app.product', 'app.category');

	function startTest() {
		$this->TransactionDetails =& new TestTransactionDetailsController();
		$this->TransactionDetails->constructClasses();
	}

	function endTest() {
		unset($this->TransactionDetails);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
