<?php
class TransactionsController extends AppController {

	var $name = 'Transactions';

	function index($page=1,$sort=null,$direction=null,$from=null,$to=null) {
		$this->Transaction->recursive = 2;
		$args = $this->passedArgs;
		if(isset($_GET['from'])) $from = $_GET['from'];
		if(isset($_GET['to'])) $to = $_GET['to'];
		foreach($args as $key=>$value){
			$$key = $value;
		}
		if($from&&$to){
			$conditions = array(
					'Transaction.transaction_date >='=>$from,
					'Transaction.transaction_date <='=>$to
			);
		}
		$conditions['Transaction.type']='sales';
		$this->paginate = array('Transaction'=>array('conditions' => $conditions,'page'=>$page,'sort'=>$sort,'direction'=>$direction,'cache'=>'default'));		
				
		$this->set('transactions', $this->paginate());
		$this->set(compact('from','to','sort','direction'));
		
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid transaction', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('transaction', $this->Transaction->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Transaction->create();
			$user = $this->Auth->user();
			$this->data['Transaction']['username'] = $user['User']['username'];
			if ($this->Transaction->saveAll($this->data)) {
				$flag=0;
				$type = $this->data['Transaction']['type'];
				switch($type){
					case 'sales':
						$flag = -1;
					break;
					case 'deliveries':
						$flag = 1;
					break;
				}
				foreach($this->data['TransactionDetail'] as $detail){
					$id = $detail['product_id'];
					$qty = $detail['quantity'];
					$this->Transaction->TransactionDetail->Product->updateQuantity($id,$qty*$flag);
					if($type=='deliveries'){
						$capital = $detail['price'];
						$this->Transaction->TransactionDetail->Product->updatePricing($id,$capital);
					}
				}
				$this->Session->setFlash(__('The transaction has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.', true));
			}
		}
	}
	function sales(){
		$products = $this->Transaction->TransactionDetail->Product->find('list');
		$this->set(compact('products'));
	}
	function deliveries(){
		$products = $this->Transaction->TransactionDetail->Product->find('list');
		$this->set(compact('products'));
	}
	function report(){
		$args =  $_GET;
		$conditions = array();
		$sort = array();
		foreach($args as $key=>$value){
			if($value){
				switch($key){
					case 'from':
						$conditions['transaction_date >='] = $value;
					break;
					case 'to':
						$conditions['transaction_date <='] = $value;
					break;
					default:
						$$key = $value;
					break;
				}
			}
		}
		if($sort&&$direction){
			$sort=array($sort=>$direction);
		}
		$conditions['type']='sales';
		$this->Transaction->recursive=2;
		$data = $this->Transaction->find('all',compact('conditions','sort'));
		$this->set(compact('data'));
	}
	function invoice($id){
		$this->Transaction->recursive=2;
		$data =  $this->Transaction->findById($id);
		$this->set(compact('data'));
	}
	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid transaction', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Transaction->save($this->data)) {
				$this->Session->setFlash(__('The transaction has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Transaction->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for transaction', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Transaction->delete($id)) {
			$this->Session->setFlash(__('Transaction deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Transaction was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
