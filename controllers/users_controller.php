<?php
class UsersController extends AppController {

	var $name = 'Users';
	function beforeFilter(){
		$this->Auth->allow('add');
		parent::beforeFilter();
	}
	function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			//$this->data['User']['password'] = $this->Auth->password($this->data['User']['password']);
			$this->User->create();
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
			}
		}
		$modules = $this->User->Module->find('list');
		$this->set(compact('modules'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
		$modules = $this->User->Module->find('list');
		$this->set(compact('modules'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function profile(){
		if(!empty($this->data)){
			$user = $this->Auth->user();
			$uid =$user['User']['id'];
			$pwd =$this->data['User']['password'];
			$conditions = array(
				'User.id'=>$uid,
				'User.password'=>$pwd,
			);
			$this->data['User']['id'] = $uid;
			$check = $this->User->find('count',compact('conditions'));
			if(!$check){
				$this->Session->setFlash(__('Invalid password. Please, try again.', true));
			}else{
				if ($this->User->save($this->data)) {
					$this->Session->setFlash(__('The user has been saved', true));
				} else {
					$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
				}
			}
			unset($this->data['User']['password']);
		}else{
			$this->data = $this->Auth->user();
		}
	}
	function password(){
		if(!empty($this->data)){
			$user = $this->Auth->user();
			$uid =$user['User']['id'];
			$pwd =$this->Auth->password($this->data['User']['old_password']);
			$conditions = array(
				'User.id'=>$uid,
				'User.password'=>$pwd,
			);
			$this->data['User']['id'] = $uid;
			$check = $this->User->find('count',compact('conditions'));
			if(!$check){
				$this->Session->setFlash(__('Invalid password. Please, try again.', true));
			}else{
				$this->data['User']['password'] = $this->Auth->password($this->data['User']['new_password']);
				if ($this->User->save($this->data)) {
					$this->Session->setFlash(__('The user has been saved', true));
				} else {
					$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
				}
			}
			unset($this->data['User']['password']);
		}
		$this->redirect(array('action'=>'profile'));
	}
	function login(){}
	function logout() {
        $this->redirect($this->Auth->logout());
    }
}
