<?php
/* Product Fixture generated on: 2016-01-25 02:30:00 : 1453685400 */
class ProductFixture extends CakeTestFixture {
	var $name = 'Product';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'category_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 4, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status' => array('type' => 'string', 'null' => false, 'default' => 'active', 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'particular' => array('type' => 'string', 'null' => true, 'default' => ' ', 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'part_no' => array('type' => 'string', 'null' => true, 'default' => ' ', 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'unit' => array('type' => 'string', 'null' => true, 'default' => ' ', 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'description' => array('type' => 'string', 'null' => true, 'default' => ' ', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'capital' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '8,2'),
		'markup' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '8,2'),
		'srp' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '8,2'),
		'tmp_srp' => array('type' => 'float', 'null' => false, 'default' => NULL, 'length' => '10,2'),
		'soh_quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'tmp_quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'min_quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'max_quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'discountable' => array('type' => 'boolean', 'null' => true, 'default' => NULL),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'category_id' => 'Lo',
			'status' => 'Lorem ip',
			'particular' => 'Lorem ipsum dolor sit amet',
			'part_no' => 'Lorem ipsum dolor sit amet',
			'unit' => 'Lorem ip',
			'description' => 'Lorem ipsum dolor sit amet',
			'capital' => 1,
			'markup' => 1,
			'srp' => 1,
			'tmp_srp' => 1,
			'soh_quantity' => 1,
			'tmp_quantity' => 1,
			'min_quantity' => 1,
			'max_quantity' => 1,
			'discountable' => 1,
			'created' => '2016-01-25 02:30:00',
			'modified' => '2016-01-25 02:30:00'
		),
	);
}
