var app =  angular.module('SalesModule',[]);
const DEFAULT_QUANTITY = 1;
const DEFAULT_DECIMAL = 2;
const CLOCK_UPDATE_INTERVAL = 30000;
const BASE_URL = window.location.origin + location.pathname.replace('/transactions/sales','')+'/';
app.controller('SalesController',function($scope,$filter,$interval, $http){
	$scope.initialize = function(){
		$scope.Products = [];
		loadProducts(1);
		initializeAmounts();
		initializeClock();
		$scope.$watchCollection('TransactionDetails',updateTotalAmount);
		$scope.$watch('NetAmount',updateChangeAmount);
		$scope.$watch('CashAmount',updateChangeAmount);
		$scope.$watch('TotalAmount',updateNetAmount);
		$scope.$watch('TaxAmount',updateNetAmount);
		$scope.$watch('TransacDate',formatTimestamp);
		$scope.$watch('TransacTime',formatTimestamp);
	}
	$scope.selectProduct = function(){
		var product = $filter('filter')($scope.Products, {id: $scope.Product}, true)[0];
		product.quantity = DEFAULT_QUANTITY;
		product = computeAmount(product);
		if(product){
			$scope.TransactionDetails.push(product);
		}
	}
	$scope.updateAmount = function(product){
		product = computeAmount(product);
		updateTotalAmount();
		return product;
	}
	$scope.removeItem = function(index){
		$scope.TransactionDetails.splice(index,1);
	}
	$scope.processSales = function(){
		var cash =  parseFloat($scope.CashAmount);
		var net =  parseFloat($scope.NetAmount);
		if(net==0) return alert('Oopps! No due yet.');
		if(cash<net) return alert('Oopps! Payment is less than due.');
		var url = BASE_URL+'transactions/add';
		var formData = $('#TransactionAddForm').serialize();
		if(confirm('Are you sure you want to save this transaction?')){
		$.ajax({
			type        : 'POST',
			url         : url,
			data        : formData,
			success		: function(response){ 
							$scope.$apply(initializeAmounts);
							alert(response.meta.message);
							window.open(BASE_URL+'transactions/invoice/'+response.data.id);
						},
			error		: function(response){
							alert(response.meta.message);
						},
		});
		}
	}
	function formatTimestamp(){
		var formattedDate = $scope.TransacDate? $scope.TransacDate.getFullYear()+'-'+($scope.TransacDate.getMonth()+1)+'-'+$scope.TransacDate.getDate():null;
		var formattedTime = $scope.TransacTime? $scope.TransacTime.getHours()+':'+$scope.TransacTime.getMinutes()+':00':null;
		$scope.TransacFormattedDate = formattedDate; 
		$scope.TransacFormattedTime = formattedTime; 
	}
	function initializeAmounts(){
		$scope.TransactionDetails = [];
		$scope.TotalAmount = 0;
		$scope.CashAmount = 0;
		$scope.ChangeAmount = 0;
		$scope.TaxAmount = 0;
		$scope.NetAmount = 0;
		$scope.Product = null;
	}
	function initializeClock(){
		$scope.TransacDate = new Date();
		$scope.TransacTime = formattedTime(new Date());
		$interval(function(){
			$scope.TransacTime =  formattedTime(new Date());
		},CLOCK_UPDATE_INTERVAL);
		function formattedTime(timestamp){
			timestamp.setSeconds(null,null);
			return timestamp;
		}
	}
	function updateTotalAmount(){
		var total = 0;
		for(var index in $scope.TransactionDetails){
			var detail = $scope.TransactionDetails[index];
			total += detail.amount;
		}
		$scope.TotalAmount = total;
	}
	function updateNetAmount(){
		var tax = 0;
		var gross =  $scope.TotalAmount;
		if($scope.TaxAmount) tax = parseFloat($scope.TaxAmount);
		$scope.NetAmount  = gross + tax;
	}
	function updateChangeAmount(){
		var cash =  0;
		var net =  $scope.NetAmount;
		if($scope.CashAmount) cash = parseFloat($scope.CashAmount);
		$scope.ChangeAmount = cash==0? 0: cash - net;
		
	}
	function computeAmount(product){
		if(product.soh_quantity<product.quantity){
			product.quantity = product.soh_quantity;
			alert("Item selected is low in stock. Only "+product.soh_quantity+ " in quantity.");
			return false;
		}
		var amount  = product.quantity * product.price;
		product.amount = amount;
		product.display_amount =  $filter('number')(amount, DEFAULT_DECIMAL);
		return product;
	}
	function loadProducts(page){
		var url = BASE_URL+'products.json?page='+page+'&limit=500';
		$http.get(url).success(function(response){
			$scope.Products = $scope.Products.concat(response.data);
			if(response.meta.next)
				return loadProducts(response.meta.next);
		});
	}
});