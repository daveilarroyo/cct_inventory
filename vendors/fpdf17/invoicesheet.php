<?php
	require ('formsheet.php');
	class InvoiceSheet extends Formsheet{
		public function __construct(){
			$this->_width  /=2;
			$this->_height /=2;
			parent::__construct();
		}
		function createSheet(){
			parent::createSheet();
			$this->showLines = false;
			$this->createGrid(0,0,$this->_width,$this->_height,25,15);
			$this->GRID['font_size']=16;
			$this->centerText(0,1.5,'J & R Trading',15,'b');
			$this->GRID['font_size']=10;
			$this->centerText(0,2.25,'San Roque Sto. Tomas Batangas',15);
			$this->centerText(0,3,'Tel. No (043) 788-0786',15);
			$this->GRID['font_size']=14;
			$this->leftText(1,4.5,'SALES INVOICE',15,'b');
			$this->leftText(10,4.5,'Date:',15,'b');
			$this->DrawLine(4.5,'h',array(12,2.5));
		}
		function showTable($data){
			$x = 0.5;
			$y=5.75;
			$this->GRID['font_size']=10;
			$this->rightText(12.5,4.33,$data['Transaction']['transaction_date'],3);
			$this->GRID['font_size']=11;
			$this->centerText($x+0.5,$y,'Product',2,'b');
			$this->centerText($x+4,$y,'Quantity',2,'b');
			$this->centerText($x+7,$y,'Price',3,'b');
			$this->centerText($x+11,$y,'Amount',3,'b');
			$y++;
			$this->GRID['font_size']=10;
			foreach($data['TransactionDetail'] as $detail){
				$product =  $detail['Product']['particular'];
				$user =  $detail['Transaction']['username'];
				$quantity =  $detail['quantity'];
				$price =  $detail['price'];
				$amount =  $detail['amount'];
				$date =  $detail['Transaction']['transaction_date'];
				$this->leftText($x+0.5,$y,$product,2);
				$this->centerText($x+4,$y,$quantity,2);
				$this->rightText($x+6.5,$y,$price,3);
				$this->rightText($x+10.5,$y,$amount,3);
				$y+=0.75;
			}
			
			$this->DrawLine($y-0.5,'h',array(11.5,3));
			$y+=0.25;
			$this->rightText($x+10.5,$y,$this->formatAmount($data['Transaction']['total_amount']),3,'bu');	
			$y = 20;
			$this->GRID['font_size']=12;
			$this->rightText($x+7,$y,'Total',3,'b');
			$this->rightText($x+10.5,$y,$this->formatAmount($data['Transaction']['total_amount']),3,'b');	
			$this->GRID['font_size']=9;
			$y+=0.75;
			$this->rightText($x+7,$y,'Tax',3.33,null);
			$this->rightText($x+10.5,$y,$this->formatAmount($data['Transaction']['tax_amount']),3);
			$y+=0.75;
			$this->rightText($x+7,$y,'Net',3,null);
			$this->rightText($x+10.5,$y,$this->formatAmount($data['Transaction']['net_amount']),3);				
			$y+=0.75;
			$this->rightText($x+7,$y,'Cash',3,null);
			$this->rightText($x+10.5,$y,$this->formatAmount($data['Transaction']['cash_amount']),3);
			$y+=0.75;
			$this->rightText($x+7,$y,'Change',3,null);
			$change = $data['Transaction']['cash_amount'] - $data['Transaction']['net_amount'];
			$this->rightText($x+10.5,$y,$this->formatAmount($change),3);			
		}
		function formatAmount($amount){
			return number_format($amount,2,'.',',');
		}
	}
?>