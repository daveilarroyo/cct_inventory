<?php
class Product extends AppModel {
	var $name = 'Product';
	var $displayField = 'particular';
	var $validate = array(
		'status' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => true, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	function beforeValidate($data){
		if(isset($this->data['Product']['particular'])){
			if(isset($this->data['Product']['id']))  return true;
			$conditions=array(
				'TRIM(particular)'=>trim($this->data['Product']['particular']),
				'TRIM(part_no)'=>trim($this->data['Product']['part_no']),
				'TRIM(description)'=>trim($this->data['Product']['description']),
			);
			$count = $this->find('count',compact('conditions'));
			$hasDuplicate  = $count==0;
			return $hasDuplicate;
		}else{
			return true;
		}
	}
	function updateQuantity($id,$quantity){
		$product = $this->findById($id, array('id','soh_quantity'));
		$product['Product']['soh_quantity'] = $product['Product']['soh_quantity'] + $quantity;
		$this->save($product);
	}
	function updatePricing($id,$capital){
		$product = $this->findById($id, array('id','capital','markup','price'));
		$product['Product']['capital'] = $capital;
		$product['Product']['price'] = $capital + $product['Product']['markup'];
		$this->save($product);
	}
}
