<?php
	echo $this->Form->create('Transaction',array('controller'=>'transactions','action'=>'print','type'=>'get'));
	echo $this->Form->input('sort',array('value'=>$sort,'type'=>'hidden'));
	echo $this->Form->input('direction',array('value'=>$direction,'type'=>'hidden'));
	echo $this->Form->input('from',array('value'=>$from,'type'=>'hidden'));
	echo $this->Form->input('to',array('value'=>$to,'type'=>'hidden'));
	echo $this->Form->submit('Print');
	echo $this->Form->end();
?>