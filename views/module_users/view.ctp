<div class="moduleUsers view">
<h2><?php  __('Module User');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $moduleUser['ModuleUser']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($moduleUser['User']['username'], array('controller' => 'users', 'action' => 'view', $moduleUser['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Module'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($moduleUser['Module']['name'], array('controller' => 'modules', 'action' => 'view', $moduleUser['Module']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Module User', true), array('action' => 'edit', $moduleUser['ModuleUser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Module User', true), array('action' => 'delete', $moduleUser['ModuleUser']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $moduleUser['ModuleUser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Module Users', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module User', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Modules', true), array('controller' => 'modules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module', true), array('controller' => 'modules', 'action' => 'add')); ?> </li>
	</ul>
</div>
