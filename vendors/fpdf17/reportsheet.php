<?php
	require ('formsheet.php');
	class ReportSheet extends Formsheet{
		
		function createSheet(){
			parent::createSheet();
			$this->showLines = false;
			$this->createGrid(0,0,$this->_width,$this->_height,25,15);
			$this->GRID['font_size']=18;
			$this->centerText(0,1.5,'J & R Trading',15,'b');
			$this->GRID['font_size']=12;
			$this->centerText(0,2,'San Roque Sto. Tomas Batangas',15);
			$this->centerText(0,2.5,'Tel. No (043) 788-0786',15);
		}
		function showTable($data){
			$x = 0.5;
			$y=3.75;
			$this->centerText($x,$y,'Product',2,'b');
			$this->centerText($x+2,$y,'Encoder',2,'b');
			$this->centerText($x+4,$y,'Quantity',2,'b');
			$this->centerText($x+6,$y,'Price',3,'b');
			$this->centerText($x+9,$y,'Amount',3,'b');
			$this->centerText($x+12,$y,'Date',1.5,'b');
			$y++;
			foreach($data as $datum){
				foreach($datum['TransactionDetail'] as $detail){
					$product =  $detail['Product']['particular'];
					$user =  $datum['Transaction']['username'];
					$quantity =  $detail['quantity'];
					$price =  $detail['price'];
					$amount =  $detail['amount'];
					$date =  $datum['Transaction']['transaction_date'];
					$this->centerText($x,$y,$product,2);
					$this->centerText($x+2,$y,$user,2);
					$this->centerText($x+4,$y,$quantity,2);
					$this->centerText($x+6,$y,$price,3);
					$this->centerText($x+9,$y,$amount,3);
					$this->centerText($x+12,$y,$date,1.5);
					$y+=0.75;
				}
				
			}
		}
	}
?>