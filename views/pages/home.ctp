<div class="index">
<h1>Welcom to J&R Trading Inventory System!</h1>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
	<?php if($user):?>
	<?php foreach($user['Module'] as $module):?>
	<li><?php echo $this->Html->link(__($module['name'], true), array('controller'=>null,'action'=>$module['url'])); ?></li>
	<?php endforeach;?>
	<?php endif; ?>
	<li><?php echo $this->Html->link(__('Profile', true), array('controller'=>'users','action' => 'profile')); ?></li>
	<li><?php echo $this->Html->link(__('Log out', true), array('controller'=>'users','action' => 'logout')); ?></li>
	</ul>
</div>
<div class="index"> 
	<?php if($user):?>
	<?php 
		$products = $this->requestAction('products?status=low'); 
	?>
	<h2><?php __('Products');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo ('id');?></th>
			<th><?php echo ('category');?></th>
			<th><?php echo ('status');?></th>
			<th><?php echo ('particular');?></th>
			<th><?php echo ('part no');?></th>
			<th><?php echo ('unit');?></th>
			<th><?php echo ('description');?></th>
			<th><?php echo ('unit cost');?></th>
			<th><?php echo ('markup');?></th>
			<th><?php echo ('price');?></th>
			<th><?php echo ('stock on hand');?></th>
			<th><?php echo ('min quantity');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	
	<tr>
		<td colspan="13">
			<?php if(count($products)==0):?> 
				No out of stock product.
			<?php else:?>
				<p style="color:red;"><b>Alert:</b> Out of stock product(s)!</p>
			<?php endif;?>
		</td>
	</tr>
	
	<?php
	$i = 0;
	foreach ($products as $product):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $product['Product']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($product['Category']['name'], array('controller' => 'categories', 'action' => 'view', $product['Category']['id'])); ?>
		</td>
		<td><?php echo $product['Product']['status']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['particular']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['part_no']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['unit']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['description']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['capital']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['markup']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['price']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['soh_quantity']; ?>&nbsp;</td>
		<td><?php echo $product['Product']['min_quantity']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('controller'=>'products','action' => 'view', $product['Product']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	<?php else: ?>
		<?php echo $this->Form->create('User',array('action'=>'login')); ?>
		<?php echo $this->Form->input('username');?>
		<?php echo $this->Form->input('password');?>
		<?php echo $this->Form->submit('Login');?>
		<?php echo $this->Form->end();?>
	<?php endif; ?>
</div>
