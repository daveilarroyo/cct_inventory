var app =  angular.module('ProductModule',[]);
app.controller('ProductController',function($scope){
	$scope.capital = 0;
	$scope.markup = 0;
	$scope.$watch('capital',updatePrice);
	$scope.$watch('markup',updatePrice);
	function updatePrice(){
		var capital = 0;
		var markup = 0;
		if($scope.capital) capital =  parseFloat($scope.capital);
		if($scope.markup) markup =  parseFloat($scope.markup);
		$scope.price =capital+markup;
	}
	
});