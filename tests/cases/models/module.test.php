<?php
/* Module Test cases generated on: 2016-02-07 09:01:45 : 1454832105*/
App::import('Model', 'Module');

class ModuleTestCase extends CakeTestCase {
	var $fixtures = array('app.module', 'app.module_user');

	function startTest() {
		$this->Module =& ClassRegistry::init('Module');
	}

	function endTest() {
		unset($this->Module);
		ClassRegistry::flush();
	}

}
