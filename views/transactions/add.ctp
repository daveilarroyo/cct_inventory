<div class="transactions form">
<?php echo $this->Form->create('Transaction');?>
	<fieldset>
		<legend><?php __('Add Transaction'); ?></legend>
	<?php
		echo $this->Form->input('type');
		echo $this->Form->input('total_amount');
		echo $this->Form->input('tax_amount');
		echo $this->Form->input('cash_amount');
		echo $this->Form->input('net_amount');
		echo $this->Form->input('transaction_date');
		echo $this->Form->input('transaction_time');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Transactions', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Transaction Details', true), array('controller' => 'transaction_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction Detail', true), array('controller' => 'transaction_details', 'action' => 'add')); ?> </li>
	</ul>
</div>