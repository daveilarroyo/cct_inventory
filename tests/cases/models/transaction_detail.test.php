<?php
/* TransactionDetail Test cases generated on: 2016-01-29 14:21:38 : 1454073698*/
App::import('Model', 'TransactionDetail');

class TransactionDetailTestCase extends CakeTestCase {
	var $fixtures = array('app.transaction_detail', 'app.transaction', 'app.product', 'app.category');

	function startTest() {
		$this->TransactionDetail =& ClassRegistry::init('TransactionDetail');
	}

	function endTest() {
		unset($this->TransactionDetail);
		ClassRegistry::flush();
	}

}
