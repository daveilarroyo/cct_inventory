<?php
/* ModuleUsers Test cases generated on: 2016-02-07 09:03:54 : 1454832234*/
App::import('Controller', 'ModuleUsers');

class TestModuleUsersController extends ModuleUsersController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ModuleUsersControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.module_user', 'app.user', 'app.module');

	function startTest() {
		$this->ModuleUsers =& new TestModuleUsersController();
		$this->ModuleUsers->constructClasses();
	}

	function endTest() {
		unset($this->ModuleUsers);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
