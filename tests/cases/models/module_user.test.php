<?php
/* ModuleUser Test cases generated on: 2016-02-07 09:03:43 : 1454832223*/
App::import('Model', 'ModuleUser');

class ModuleUserTestCase extends CakeTestCase {
	var $fixtures = array('app.module_user', 'app.user', 'app.module');

	function startTest() {
		$this->ModuleUser =& ClassRegistry::init('ModuleUser');
	}

	function endTest() {
		unset($this->ModuleUser);
		ClassRegistry::flush();
	}

}
