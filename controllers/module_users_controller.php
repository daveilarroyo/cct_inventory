<?php
class ModuleUsersController extends AppController {

	var $name = 'ModuleUsers';

	function index() {
		$this->ModuleUser->recursive = 0;
		$this->set('moduleUsers', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid module user', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('moduleUser', $this->ModuleUser->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ModuleUser->create();
			if ($this->ModuleUser->save($this->data)) {
				$this->Session->setFlash(__('The module user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The module user could not be saved. Please, try again.', true));
			}
		}
		$users = $this->ModuleUser->User->find('list');
		$modules = $this->ModuleUser->Module->find('list');
		$this->set(compact('users', 'modules'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid module user', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ModuleUser->save($this->data)) {
				$this->Session->setFlash(__('The module user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The module user could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ModuleUser->read(null, $id);
		}
		$users = $this->ModuleUser->User->find('list');
		$modules = $this->ModuleUser->Module->find('list');
		$this->set(compact('users', 'modules'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for module user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ModuleUser->delete($id)) {
			$this->Session->setFlash(__('Module user deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Module user was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
